#!/bin/bash

#check files
if [ ! -e dump.sql ]
then
  echo "can not find dump.sql"
  exit
fi
#init
docker-compose up -d
#fix zabbix user error
sudo docker exec -it zabbix-server_zabbix-web_1 apk add php7-fileinfo
docker-compose restart zabbix-web
#setup zabbix alert script environment
sudo docker exec -it zabbix-server_zabbix-server_1 apk add git
sudo docker exec -it zabbix-server_zabbix-server_1 apk add py-pip
sudo docker exec -it zabbix-server_zabbix-server_1 pip install -r /var/lib/zabbix/requirements.txt
